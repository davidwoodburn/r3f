---
title: 'r3f: Rotation of 3-dimensional Reference Frames'
tags:
  - Python
  - reference frames
  - coordinate frames
authors:
  - name: David Woodburn
    orcid: 0009-0000-5935-2850
    equal-contrib: false
    affiliation: 1
affiliations:
 - name: Air Force Institute of Technology, USA
   index: 1
date: 03 October 2024
bibliography: paper.bib
---

# Summary

The `r3f` library includes attitude-representation conversions, reference-frame
conversions, and rotation matrix (direction cosine matrix) utilities. All twenty
possible conversions among the following five attitude representations are
provided: rotation vector, rotation axis and angle, roll and pitch and yaw (RPY)
Euler angles, direction cosine matrix (DCM), and quaternion. This library also
includes all twelve possible conversions among the following four frames: ECEF
(Earth-centered, Earth-fixed), geodetic (latitude, longitude, and height above
ellipsoid), local-level tangent, and local-level curvilinear. Both local-level
frames can be in either NED (North, East, Down) or ENU (East, North, Up)
orientation. A general `dcm` function will generate any arbitrary rotation
matrix based on an arbitrary sequence of Euler rotation angles. This can be used
to convert between ECEF and ECI (Earth-centered Inertial) frames.

A few additional utility functions are included. The `orthonormalize_dcm`
function will normalize and orthogonalize a rotation matrix. The two Rodrigues's
rotation functions, `rodrigues_rotation` and `inverse_rodrigues_rotation`, are
meant for converting a vector to the matrix exponential of the skew-symmetric
matrix of that vector and back again.

This library can be installed with [`pip install
r3f`](https://pip.pypa.io/en/stable/getting-started/).

# Statement of need

Navigation algorithms heavily rely on reference frame conversions and various
attitude representations. Key resources, such as the works of @titterton2004 and
@groves2013, cover inertial navigation, global navigation satellite systems, and
other navigation instruments, emphasizing these conversions and representations.
The r3f library offers efficient tools for handling these tasks, with functions
capable of processing individual vectors or batches of vectors stored as
matrices. Built on NumPy [@harris2020array], `r3f` maintains simplicity with a
single dependency.

The following is a list of some other Python libraries with similar
functionality. Except for `gps-frames` it does not appear that any of these has
both the standard Earth reference frame conversions as well as attitude
representation conversions.

| Name          | Author        | License |
| ------------- | ------------- | ------- |
| `PyMap3D`     | @Hirsch2018   | BSD     |
| `transforms`  | @transforms   | GNU GPL |
| `gps-frames`  | @gpsframes    | GNU GPL |
| `rotations`   | @rotations    | MIT     |
| `gtFrame`     | @gtFrame      | MIT     |
| `PyGeodesy`   | @PyGeodesy    | MIT     |

Here is an example in which a flight path defined in a local-level curvilinear
frame is converted to geodetic coordinates:

```python
    import numpy as np
    import r3f

    K = 1000
    theta = np.linspace(np.pi, -np.pi, K)
    xc = 1000e3 * (1 + np.cos(theta))
    yc = 1000e3 * np.sin(theta)
    zc = -(np.cos(theta) + 1) * 1e3/2

    [lat, lon, hae] = r3f.curvilinear_to_geodetic(
            [xc, yc, zc],
            [39.783, -84.083, 317.0], degs=True)
```

This library is currently used by students at the Air Force Institute of
Technology and in research software.

# References
