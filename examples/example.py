import numpy as np
import r3f

# Define geodetic coordinates of the origin.
lat0 = 39.783
lon0 = -84.083
hae0 = 317.0

# Define geodetic coordinates of another point.
lat = 40.0
lon = -84.0
hae = 300.0

# Convert geodetic coordinates to ECEF.
[xe0, ye0, ze0] = r3f.geodetic_to_ecef([lat0, lon0, hae0], degs=True)
[xe, ye, ze] = r3f.geodetic_to_ecef([lat, lon, hae], degs=True)
print(f"ECEF of origin: {xe0}, {ye0}, {ze0}")
print(f"ECEF of point:  {xe}, {ye}, {ze}")

# Convert to local-level tangent frame.
[xt, yt, zt] = r3f.ecef_to_tangent([xe, ye, ze], [xe0, ye0, ze0])
print(f"ECEF to tangent: {xt}, {yt}, {zt}")

# Convert tangent back to ECEF.
[xe, ye, ze] = r3f.tangent_to_ecef([xt, yt, zt], [xe0, ye0, ze0])
print(f"Tangent to ECEF:  {xe}, {ye}, {ze}")

# Convert ECEF coordinates to geodetic.
[lat, lon, hae] = r3f.ecef_to_geodetic([xe, ye, ze], degs=True)
print(f"ECEF to geodetic: {lat}, {lon}, {hae}")
