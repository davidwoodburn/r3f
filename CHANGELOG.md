# CHANGELOG

## 2.1.6 (2025-02-06)

### New

*   Paper published! DOI added.

## 2.1.5 (2025-02-05)

### Fixed

*   Added citation for Numpy.

## 2.1.4 (2025-02-04)

### New

*   Moved the contribution to a separate file.

## 2.1.3 (2025-01-28)

### New

*   Added multiple tests for each function in the docstrings.
*   Changed the name of `rot` to `dcm` to more clearly distinguish from
    `rotate`.
*   Opened the project for forking and merge requests. Add corresponding note in
    README.

### Fixed

*   Passing `doctest`.
*   Passing `pylint` (disabled:invalid-name, too-many-lines, too-many-locals,
    too-many-branches, multiple-statements)
*   Passing all tests with `pytest`.
*   Full test coverage using `coverage`.
*   Passing `mypy`.
*   Added optional dependency of `pytest`.

## 2.1.2 (2025-01-09)

### New

*   Added examples folder.

## 2.1.1 (2025-01-09)

### Fixed

*   More detailed contribution section.

## 2.1.0 (2025-01-03)

### Fixed

*   Scaling problem with the `dcm_to_rpy` function.
*   Removed `FIXME` from `test_r3f.py` after verifying success with 1 million
    random sets.
*   Removed import of matplotlib in `test_r3f.py`.

### New

*   Added `euler` function to get back the Euler angles from a rotation matrix.

## 2.0.17 (2024-12-13)

### New

*   Added type hints.
*   Condensed some of the parsing lines into a single line.

## 2.0.16 (2024-12-10)

### Fixed

*   Changed folder structure.

## 2.0.15 (2024-12-07)

### Fixed

*   Division by zero and masking in rodrigues functions.

## 2.0.14 (2024-11-13)

### Fixed

*   Added distribution statement to LICENSE.

## 2.0.13 (2024-11-12)

### Fixed

*   Added example to paper.

## 2.0.12 (2024-11-08)

### Fixed

*   Missing "Statement of need" section in paper.
*   Added comments to README.

## 2.0.11 (2024-10-21)

### New

*   Added list of functions to r3f doc string.

## 2.0.10 (2024-10-21)

### New

*   Added list of constants and functions to README.

## 2.0.9 (2024-10-03)

### New

*   Draft paper for Journal of Open Source Software

## 2.0.8 (2024-08-19)

### Fixed

*   Mistaken directory.

## 2.0.7 (2024-08-19)

### Fixed

*   Added the CHANGELOG.md and LICENSE.txt file back in.

## 2.0.6 (2024-08-19)

### Fixed

*   Removed metadata from `r3f.py` in favor of `pyproject.toml`.

## 2.0.5 (2024-05-29)

### New

*   `rotate()` function will apply a stack of rotations to a matrix of vectors.

## 2.0.4 (2024-05-28)

### New

*   `rot()` now accepts strings for specifying the axes of rotation, `ax`.

### Fixed

*   Fixed bug where `rot()` failed if angle of first rotation was `0`.
