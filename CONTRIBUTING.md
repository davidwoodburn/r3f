## Contribution and Support

Before submitting an issue or merge request, check if it has already been
addressed. Thank you for helping us improve the software!

### Issue Submission

We value your feedback! To report a bug, request a feature, or ask a question,
please submit an issue [here][issues].

For bug reports, please provide

-   Package version, Python version, and operating system
-   Minimal working example and expected behavior

For feature requests, describe the desired functionality and include an example
of its use.

### Direct Contributions

If you would like to directly contribute,

-   **Fork the Repository:** Create a fork of this repository.
-   **Make Changes:** Work on your changes in a feature branch.
-   **Submit a Merge Request:** Open a merge request (MR) with a clear
    description of your changes.

Before submitting, ensure your contributions

-   Follow the project's code style.
-   Include relevant tests and documentation.


[issues]: https://gitlab.com/davidwoodburn/r3f/-/issues